// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.motorcontrol.VictorSP;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Collector;

public class TeleopCollector extends CommandBase {
  /** Creates a new TeleopCollector. */
  Joystick joystick;
  Collector collector;
  
  public TeleopCollector(Collector mCollector, Joystick mJoystick) {
    collector = mCollector;
    joystick = mJoystick;
    addRequirements(collector);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(joystick.getRawButton(4)){
      collector.frontSpeed(0.5);
      collector.backSpeed(0.3);
    }else if(joystick.getRawButton(2)){
      collector.frontSpeed(0.34);
      collector.backSpeed(-0.08);
    }

    System.out.println(joystick.getPOV());
    if(joystick.getPOV() == 0) {
      collector.upPin();
    }else if(joystick.getPOV() == 180) {
      collector.downPin();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
