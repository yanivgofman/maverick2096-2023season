// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj.motorcontrol.VictorSP;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Collector extends SubsystemBase {
  /** Creates a new Collector. */
  TalonFX rightFront;
  TalonFX leftFront;
  TalonFX rightBack;
  TalonFX pin;

  public Collector() {
      rightFront = new TalonFX(3);
      leftFront = new TalonFX(4);
      rightBack = new TalonFX(2);
      pin = new TalonFX(1);


  }
  public void frontSpeed(double speed){
    rightFront.set(ControlMode.PercentOutput,speed);
    leftFront.set(ControlMode.PercentOutput, -speed);
  }
  public void backSpeed(double speed){
    rightBack.set(ControlMode.PercentOutput, -speed);
  }

  public void downPin() {
    pin.set(ControlMode.Position, 0);
  }

  public void upPin() {
    pin.set(ControlMode.Position, 0);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
