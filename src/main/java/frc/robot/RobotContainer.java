package frc.robot;

import com.pathplanner.lib.PathConstraints;
import com.pathplanner.lib.PathPlanner;
import com.pathplanner.lib.PathPlannerTrajectory;
import com.pathplanner.lib.PathPlannerTrajectory.PathPlannerState;
import com.pathplanner.lib.commands.PPRamseteCommand;
import com.pathplanner.lib.commands.PPSwerveControllerCommand;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

import frc.robot.autos.*;
import frc.robot.commands.*;
import frc.robot.subsystems.*;

public class RobotContainer {
    /* Controllers */
    private final Joystick driver = new Joystick(0);
    private final Joystick subdriver = new Joystick(1);

    /* Drive Controls */
    private final int translationAxis = XboxController.Axis.kLeftY.value;
    private final int strafeAxis = XboxController.Axis.kLeftX.value;
    private final int rotationAxis = 2;

    /* Driver Buttons */
    private final JoystickButton zeroGyro = new JoystickButton(driver, XboxController.Button.kY.value);
    private final JoystickButton robotCentric = new JoystickButton(driver, XboxController.Button.kLeftBumper.value);

    /* Subsystems */
    private final Swerve s_Swerve = new Swerve();
    private final Collector collector = new Collector();


    /** The container for the robot. Contains subsystems, OI devices, and commands. */
    public RobotContainer() {
        s_Swerve.setDefaultCommand(
            new TeleopSwerve(
                s_Swerve, 
                () -> -driver.getRawAxis(translationAxis), 
                () -> -driver.getRawAxis(strafeAxis), 
                () -> -driver.getRawAxis(rotationAxis), 
                () -> robotCentric.getAsBoolean(),
                driver
            )
        );
        collector.setDefaultCommand(new TeleopCollector(collector, subdriver));

        // Configure the button bindings
        configureButtonBindings();
    }

    private void configureButtonBindings() {
        /* Driver Buttons */
        zeroGyro.whenPressed(new InstantCommand(() -> s_Swerve.zeroGyro()));
    }


    public Command getAutonomousCommand() {
    
        PathPlannerTrajectory tryPath = PathPlanner.loadPath("Try1", new PathConstraints(4, 3));
        PathPlannerState pathState = (PathPlannerState) tryPath.sample(1.2);

        PPSwerveControllerCommand ramsete = new PPSwerveControllerCommand(tryPath, s_Swerve::getPose, Constants.Swerve.swerveKinematics, new PIDController(0.4, 0, 0), new PIDController(0.4, 0, 0), new PIDController(0.4, 0, 0), s_Swerve::setModuleStates, s_Swerve);
        SequentialCommandGroup all = new SequentialCommandGroup(new InstantCommand(() -> s_Swerve.resetOdometry(tryPath.getInitialHolonomicPose())),new InstantCommand(() -> s_Swerve.resetWheels()), ramsete, new InstantCommand(() -> s_Swerve.stopAllModules()));

        return all;
    }
}
